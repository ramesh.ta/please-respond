# Please Respond

> Your marketing firm is tracking popularity of events around the world. You have been tasked with tracking down RSVPs for events on a popular online service that helps to organize events, Meetup.com.

## Requirements
Collect streaming RSVP data from Meetup.com and output aggregate information about the observed Event RSVPs. 

Your program should connect to the Meetup.com and collect RSVPs to Meetup Events for a configurable time period (default 60 seconds). 

The following aggregate information should be calculated:

- Total # of RSVPs received
- Date of Event furthest into the future
- URL for the Event furthest into the future
- The top 3 number of RSVPs received per Event host-country

### Input Format
Your program should connect to the Meetup.com RSVP HTTP data stream at http://stream.meetup.com/2/rsvps

- Meetup RSVP reference: https://www.meetup.com/meetup_api/docs/stream/2/rsvps/

You may assume that the input files are correctly formatted. Error handling for invalid input files may be ommitted.

### Output Format
The output will specify the aggregate calculated information in a comma-delimited format.

```
total,future_date,future_url,co_1,co_1_count,co_2,co_2_count,co_3,co_3_count
```

The program will output to screen or console (and not to a file). 

## Sample Data
The following may be used as a sample output dataset.

### Ouput

```
100,2019-04-17 12:00,https://www.meetup.com/UXSpeakeasy/events/258247836/,us,40,uk,18,jp,12
```

## Ingesting Meetup RSVP Streaming API

### Sample response

The following is an example response received from the meetup.com streaming API endpoint. The JSON has been formatted 
with a beautifier to make human readable. From this sample response, 
the data schema of the response was determined and the internal code 
models where developed.

```json
{
   "venue":{
      "venue_name":"Online event",
      "lon":179.1962,
      "lat":-8.521147,
      "venue_id":26906060
   },
   "visibility":"public",
   "response":"yes",
   "guests":0,
   "member":{
      "member_id":243248340,
      "photo":"https:\/\/secure.meetupstatic.com\/photos\/member\/9\/f\/0\/c\/thumb_272920716.jpeg",
      "member_name":"Nur Amelia"
   },
   "rsvp_id":1855843425,
   "mtime":1605150569016,
   "event":{
      "event_name":"Parlons francais ONLINE! French Conversations",
      "event_id":"274544641",
      "time":1605330000000,
      "event_url":"https:\/\/www.meetup.com\/TheLanguageLovers\/events\/274544641\/"
   },
   "group":{
      "group_topics":[
         {
            "urlkey":"mandarin-language",
            "topic_name":"Mandarin Language"
         },
         {
            "urlkey":"mandarin-learning",
            "topic_name":"Mandarin Learning"
         },
         {
            "urlkey":"chinese-language",
            "topic_name":"Chinese Language"
         },
         {
            "urlkey":"chinese-learning",
            "topic_name":"Chinese Learning"
         },
         {
            "urlkey":"language-exchange",
            "topic_name":"Language Exchange"
         },
         {
            "urlkey":"language",
            "topic_name":"Language & Culture"
         },
         {
            "urlkey":"japanese-language",
            "topic_name":"Japanese Language"
         },
         {
            "urlkey":"russian",
            "topic_name":"Russian Language"
         },
         {
            "urlkey":"korean",
            "topic_name":"Korean Language"
         },
         {
            "urlkey":"cantonese",
            "topic_name":"Cantonese Language"
         },
         {
            "urlkey":"learn-to-speak-mandarin",
            "topic_name":"Learn to speak Mandarin"
         },
         {
            "urlkey":"learn-to-speak-cantonese",
            "topic_name":"Learn to Speak Cantonese"
         },
         {
            "urlkey":"personal-development",
            "topic_name":"Personal Development"
         },
         {
            "urlkey":"learning",
            "topic_name":"Learning"
         },
         {
            "urlkey":"self-improvement",
            "topic_name":"Self-Improvement"
         }
      ],
      "group_city":"Singapore",
      "group_country":"sg",
      "group_id":26037448,
      "group_name":"The Language Lovers",
      "group_lon":103.85,
      "group_urlname":"TheLanguageLovers",
      "group_lat":1.3
   }
}
```

package com.enlighten.rsvp.model;

import com.fasterxml.jackson.annotation.JsonProperty;


public class RSVP {

    @JsonProperty
    private Event event;
    @JsonProperty
    private Group group;
    @JsonProperty
    private Long guests;
    @JsonProperty
    private Member member;
    @JsonProperty
    private Long mtime;
    @JsonProperty
    private String response;
    @JsonProperty("rsvp_id")
    private String rsvpId;
    @JsonProperty
    private Venue venue;
    @JsonProperty
    private String visibility;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Long getGuests() {
        return guests;
    }

    public void setGuests(Long guests) {
        this.guests = guests;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getRsvpId() {
        return rsvpId;
    }

    public void setRsvpId(String rsvpId) {
        this.rsvpId = rsvpId;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

}

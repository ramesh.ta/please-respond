package com.enlighten.rsvp.configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class Configurations {

    @Bean
    public RestTemplate RestTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Configuration
    public class WebConfig implements WebMvcConfigurer {
        @Override
        public void addFormatters(FormatterRegistry registry) {
            registry.addConverter(new StringToEnumConverter());
        }
    }

    private class StringToEnumConverter implements Converter<String, TimeUnit> {
        @Override
        public TimeUnit convert(String source) {
            try {
                return TimeUnit.valueOf(source.toUpperCase());
            } catch (IllegalArgumentException e) {
                return TimeUnit.SECONDS;
            }
        }
    }
}
